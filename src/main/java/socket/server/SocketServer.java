package socket.server;

import org.springframework.stereotype.Service;
import service.MessageService;
import utils.SocketUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

@Service
public class SocketServer {
    private MessageService messageService;
    List<String> messages = new ArrayList<>();
    List<PrintWriter> clients = new ArrayList<>();
    String line;

    public void start() {
        try (ServerSocket serverSocket = new ServerSocket(SocketUtils.PORT)) {


            while (true) {
                System.out.println("Server waiting!");
                Socket socket = serverSocket.accept();
                new Thread(() -> {
                    try {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                        PrintWriter printWriter = new PrintWriter(socket.getOutputStream());

                        if (!clients.contains(printWriter)) {
                            clients.add(printWriter);
                        }
                        messages.forEach(element -> SocketUtils.sendMessage(element, printWriter));

                        while ((line = bufferedReader.readLine()) != null) {
                            System.out.println("Message received: " + line);
                            messages.add(line);
                            // messageService.saveMessageForAllUsers(line);
                            //messageService.getAllMessages();
                            new Thread(() -> {
                                for (int j = 0; j <= clients.size(); j++) {
                                    SocketUtils.sendMessage(line, clients.get(j));
                                }
                            }).start();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }).start();
                System.out.println("Request received!");
            }
        } catch (
                IOException e) {
            e.printStackTrace();
        }
    }
}



