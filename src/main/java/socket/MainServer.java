package socket;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import socket.server.SocketServer;

import java.io.IOException;

public class MainServer {


    public static void main(String[] args) throws IOException {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("annotation-context.xml");
        SocketServer socketServer = applicationContext.getBean(SocketServer.class);
        socketServer.start();
    }
}
