package socket.client;

import domain.User;
import org.springframework.stereotype.Service;
import utils.SocketUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Random;
import java.util.Scanner;

import static utils.SocketUtils.sendMessage;

@Service
public class SocketClient {

    public Socket connect() throws IOException {
        return new Socket(SocketUtils.IP, SocketUtils.PORT);
        //private MessageService messageService;
//        try (Socket socket = new Socket(SocketUtils.IP, SocketUtils.PORT)) {
//            PrintWriter printWriter = new PrintWriter(socket.getOutputStream());
//            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
//            Random random = new Random();
//            Scanner scanner = new Scanner(System.in);
//            User user = new User(random.nextInt(10));
//
//            int clientId = random.nextInt();
//
//
//            new Thread(() -> {
//                sendMessage("Client " + clientId + " : Hi Server!", printWriter);
//            }).start();
//
//            new Thread(() -> {
//                String line;
//
//                try {
//                    while ((line = bufferedReader.readLine()) != null) {
//                        System.out.println(line);
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }).start();
//            new Thread(() -> {
//                while (true) {
//                    String line = scanner.nextLine();
//                    printWriter.println(line);
//                    printWriter.flush();
//                }
//            }).start();
//            while(true) {
//
//            }
//        } catch (UnknownHostException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        //return messageService.getAllMessages();
    }
}

