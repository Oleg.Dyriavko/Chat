package socket;

import socket.client.SocketClient;

import java.io.IOException;
import java.sql.SQLException;

public class MainClient {


    public static void main(String[] args) throws IOException, SQLException {
        SocketClient socketClient = new SocketClient();
        socketClient.connect();
    }
}
