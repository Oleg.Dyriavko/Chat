package domain;

/**
 * Created by user on 10.09.2017.
 */
public class Message {
    private String subject;
    private int userId;

    public Message(String subject, int userId) {
        this.subject = subject;
        this.userId = userId;
    }

    public Message(String subject) {
        this.subject = subject;
    }
}
