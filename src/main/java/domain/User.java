package domain;

/**
 * Created by user on 10.09.2017.
 */

import lombok.AllArgsConstructor;
import lombok.Getter;

//@AllArgsConstructor
@Getter


public class User {
    private int id;
    private String firstName;
    private String lastName;

    public User(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public User(int id, String firstName, String lastName) {
        this(firstName, lastName);
        this.id = id;
    }

    public User(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }
}
