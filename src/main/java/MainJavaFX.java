import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

/**
 * Created by user on 19.09.2017.
 */
public class MainJavaFX extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        System.out.println("Started");
        primaryStage.setTitle("GUI Application");

        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("main.fxml"));

        Scene scene=new Scene(root, 800, 600);

        primaryStage.setScene(scene);

        primaryStage.show();

    }
    public static void main(String[] args) {
        launch(args);
    }
}
