package controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import service.MessageService;
import socket.client.SocketClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by user on 19.09.2017.
 */
public class MainController {

    private Socket socket = new SocketClient().connect();

    private PrintWriter printWriter = new PrintWriter(socket.getOutputStream());

    MessageService messageService;
    @FXML
    private TextArea chatWindow;

    @FXML
    private TextField messageField;

    @FXML
    private Button sendButton;

    @FXML
    public void initialize() {
        new Thread(() -> {
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    chatWindow.appendText(line + '\n');
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public MainController() throws IOException {
    }


    @FXML
    public void sendMessage() {
        printWriter.println(messageField.getText());
        printWriter.flush();
    }

}
