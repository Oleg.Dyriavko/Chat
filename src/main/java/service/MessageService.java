package service;

import domain.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MessageService {
    private UserService userService;

    public void saveMessageForAllUsers(String subject) throws SQLException {
        List<User> users = userService.getAllUsers();

        try (Connection connection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/chat",
                "root",
                "root")) {
            String insertMessage = "INSERT INTO message (subject, user_id) " +
                    "VALUES(?, ?)";

            PreparedStatement statement = connection.prepareStatement(insertMessage);

            users.forEach(user -> {
                try {
                    statement.setString(1, subject);
                    statement.setInt(2, user.getId());
                    statement.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });

        }
    }

    public List<String> getAllMessages() throws SQLException {
        List<String> messages = new ArrayList<>();

        try (Connection connection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/chat",
                "root",
                "root")) {
            String query = "SELECT * FROM message";

            ResultSet resultSet = connection.createStatement().executeQuery(query);

            while (resultSet.next()) {
                String subject = resultSet.getString("subject");
                messages.add(subject);
            }
        }
        return messages;
    }

    public void print() {
        System.out.println("I'm message service");
    }
}
