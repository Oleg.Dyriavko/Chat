package learn_spring.via_xml;
import controller.MainController;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * Created by user on 21.09.2017.
 */
@Service
class TestService {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        MainController controller = (MainController) context.getBean("mainController");
        //controller.getMessageService().print();
    }
}
